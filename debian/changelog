node-fstream (1.0.12-4apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 21:53:13 +0000

node-fstream (1.0.12-4) unstable; urgency=medium

  * Team upload
  * Back to unstable after successful tests

 -- Xavier Guimard <yadd@debian.org>  Sat, 24 Oct 2020 19:54:12 +0200

node-fstream (1.0.12-3) experimental; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Use dh-sequence-nodejs
  * Add fix for mkdirp ≥ 1

 -- Xavier Guimard <yadd@debian.org>  Fri, 23 Oct 2020 09:07:13 +0200

node-fstream (1.0.12-2) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Switch install to pkg-js-tools

 -- Xavier Guimard <yadd@debian.org>  Sat, 03 Aug 2019 15:43:05 +0200

node-fstream (1.0.12-1) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 11
  * Declare compliance with policy 4.3.0
  * Change section to javascript
  * Change priority to optional
  * Add debian/gbp.conf
  * New upstream version 1.0.12 (Closes: #931408, CVE-2019-13173)
  * Add upstream/metadata
  * Switch test to pkg-js-tools
  * Update VCS fields to salsa
  * Update debian/copyright
  * Add debian/clean

 -- Xavier Guimard <yadd@debian.org>  Thu, 04 Jul 2019 11:54:07 +0200

node-fstream (1.0.10-1+deb10u1co1) apertis; urgency=medium

  [ root ]
  * Refresh the automatically detected licensing information

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 21 Feb 2021 10:40:56 +0000

node-fstream (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10
  * dh 9
  * Standards-Version 3.9.8
  * Secure Vcs url
  * XS-Testsuite now Testsuite
  * License now ISC
  * Autopkgtest instead of build test
  * Add patch to fix a simple test failure

 -- Jérémy Lal <kapouer@melix.org>  Thu, 17 Nov 2016 22:51:27 +0100

node-fstream (1.0.4-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update minimum required version for dependencies.
  * Update Homepage & watch file URL from isaacs to npm.
  * Update Vcs-Browser URL to use cgit instead of gitweb.
  * Override dh_auto_test to run tap tests, override dh_clean to remove
    deep-copy files created by tests.
  * Bump Standards-Version to 3.9.6, no changes.
  * Add gbp.conf to use pristine-tar by default.
  * Also install package.json.
  * Add autopkgtest control & require tests.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 16 Mar 2015 23:40:25 +0100

node-fstream (0.1.24-1) unstable; urgency=low

  * Upstream update.
  * Use dh_installexamples instead of dh_installdocs.
  * control:
    + Standards-Version 3.9.4
    + Canonicalize Vcs fields.
    + Bump versions of dependencies to match upstream requirements.

 -- Jérémy Lal <kapouer@melix.org>  Wed, 21 Aug 2013 00:40:03 +0200

node-fstream (0.1.22-1) experimental; urgency=low

  * Upstream update.
  * Versioned dependency on node-graceful-fs 1.2.0.
  * Use github url in watch file.

 -- Jérémy Lal <kapouer@melix.org>  Fri, 22 Mar 2013 01:18:20 +0100

node-fstream (0.1.13-1) unstable; urgency=low

  * Initial release (Closes: #664695)

 -- Jérémy Lal <kapouer@melix.org>  Sat, 17 Mar 2012 23:37:48 +0100
